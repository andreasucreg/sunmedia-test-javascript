# Test 1

Para responder a este test encontrarás un archivo llamado `test.js` en esta 
misma carpeta donde hay un pequeño fragmento de código que deberás analizar 
y responder a las siguientes cuestiones. 

1. En el fragmento de código de nuestro archivo (`test.js`) podemos encontrar
 hasta 3 variables. ¿Podrías decirnos cuál sería el valor de todas ellas al 
 finalizar la ejecución del script?

 El valor de las variables son los siguientes:

    var rgb = {
        red: "#FF0000",
        green: "#00FF00",
        blue: "#0000FF",
        white: "#FFFFFF",
        black: "#000000"
    };

    var wb = {
        white: "#FFFFFF",
        black: "#000000"
    };

    var colors = {
        red: "#FF0000",
        green: "#00FF00",
        blue: "#0000FF",
        white: "#FFFFFF",
        black: "#000000"
    };


2. Modifica el código para que las variables `rgb` y `wb` mantengan sus valores 
iniciales y `colors` tenga los valores de ambas al finalizar la ejecución del 
script.

    El código debe modificarse en la línea 12 de manera que los valores se copien 
    en una variable diferente a `rgb` como se muestra a continuación:

    var rgb = {
        red: "#FF0000",
        green: "#00FF00",
        blue: "#0000FF"
    };

    var wb = {
        white: "#FFFFFF",
        black: "#000000"
    };

    var colors = Object.assign({}, rgb, wb);

3. Además, tenemos un bug localizado en dispositivos con Internet Explorer… 
El código de nuestro script no funciona y necesitamos que se ejecute también 
en este navegador. ¿Sabrías identificar cuál es el problema? ¿Qué solución nos
 propones?

**PS**: No es estrictamente necesario tener Internet Explorer para poder identificar y/o resolver el bug. 


    El problema es que Object.assign no es compatible con Internet Explorer. 
    Existen al menos dos soluciones viables: 

        a) Utilizando jQuery:

            Reemplazar la línea 12 por
                var colors = $.extend({},rgb, wb);

        b) Utilizando Javascript se tendría que iterar los objetos y crear uno nuevo:

            var rgb = {
                red: "#FF0000",
                green: "#00FF00",
                blue: "#0000FF"
            };

            var wb = {
                white: "#FFFFFF",
                black: "#000000"
            };

            var colors = {};

            for(var key in rgb) {
                colors[key] = rgb[key];
            }

            for(var key in wb) {
                colors[key] = wb[key];
            }
