# Test 2

El fragmento de código de nuestro fichero `test.js` nos devuelve un output no 
deseado. Queremos imprimir un valor incremental a cada segundo pero lo que 
nos devuelve el código es el mismo valor en cada iteración. 

1. Sin necesidad de ejecutar el código, ¿sabrías decirnos qué valor imprimiría
 por consola el script? ¿Cuál es el motivo?

    El valor que se imprime es 5 porque el ciclo del for termina antes que transcurra el tiempo de setTimeout.

2. Sabiendo que el output que buscamos es el que encuentras bajo estas líneas… 
¿Cómo solucionarías el fragmento de código para que el output sea el deseado?

    Se puede definir la parte del timeout en un función aparte y llamarla en el ciclo. 
    De esta manera se ejecuta la función para cada valor de i y se imprimirá el correspondiente valor.


    for (var i = 0; i < 5; i++) {
        printValueWithTimeout(i);
    }

    function printValueWithTimeout(i) {
        setTimeout(function () {
            console.log(i);
        }, 1000)
    }

