export default class EventManager{
    
    constructor(events, types) {
        this.events = (events).filter(element => (types).indexOf(element.type) > -1);
        this.types = types;
        this.time = 0;
    }

    printEvent() {
        let event = (this.events).filter(element => element.second === this.time/1000);
        if( event.length > 0) {
            event = {
                'type': event[0]['type'],
                'message': event[0]['message'],
            }
            console.log("At second", this.time/1000, ":", event);
        }
        this.time = this.time + 1000;
    }

    run() {
        console.log("RUNNING");
        setInterval(this.printEvent.bind(this), 1000);
    }
};